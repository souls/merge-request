import { MergeRequest } from './../../model/merge-request';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { MergeRequestAction } from './merge-request.action';

export interface MergeRequestStateEntity extends EntityState<MergeRequest> {
    loading: boolean;
    loaded: boolean;
    logs: {
        type: string;
        message: string;
    };
}

export const MergeRequestAdapter: EntityAdapter<MergeRequest> = createEntityAdapter<MergeRequest>({
    sortComparer: false
});

export const initialState: MergeRequestStateEntity = MergeRequestAdapter.getInitialState({
    loading: false,
    loaded: false,
    logs: undefined,
});

export const {
    selectIds: selectMergeRequestIds,
    selectEntities: selectMergeRequestEntities,
    selectAll: selectMergeRequests,
    selectTotal: selectMergeRequestTotal
} = MergeRequestAdapter.getSelectors();

export function mergeRequestReducer(state = initialState, action: MergeRequestAction.Actions)
 : MergeRequestStateEntity {

    switch (action.type) {
        case MergeRequestAction.ActionType.LOAD_MERGE_REQUESTS:
            return {
                ...state,
                loading: true,
                loaded: false
            };
        case MergeRequestAction.ActionType.SUCCESS_LOAD_MERGE_REQUESTS:
            return {
                ...MergeRequestAdapter.addMany(action.payload, state),
                loading: false,
                loaded: true,
                logs: {
                    type: 'SUCCESS',
                    message: 'Success to load merge request entities'
                }
            };
        case MergeRequestAction.ActionType.ERROR_LOAD:
            return {
                ...state,
                loading: false,
                loaded: false,
                logs: {
                    type: 'ERROR',
                    message: action.payload.message
                }
            };
            case MergeRequestAction.ActionType.ADD_MERGE_REQUEST:
                return {
                    ...state,
                    loading: true,
                    loaded: false,
                };
            case MergeRequestAction.ActionType.SUCCESS_ADD_MERGE_REQUEST:
            return {
                ...MergeRequestAdapter.addOne(action.payload, state),
                loading: false,
                loaded: true,
                logs: {
                    type: 'SUCCESS',
                    message: 'Success to add merge resquest entity'
                }
            };
            case MergeRequestAction.ActionType.UPDATE_MERGE_REQUEST:
                return {
                    ...state,
                    loading: true,
                    loaded: false,
                };
            case MergeRequestAction.ActionType.SUCCESS_UPDATE_MERGE_REQUEST:
            return {
                ...MergeRequestAdapter.updateOne({id: action.payload.id, changes: action.payload}, state),
                loading: false,
                loaded: true,
                logs: {
                    type: 'SUCCESS',
                    message: 'Success to update merge resquest entity'
                }
            };
    }

}


