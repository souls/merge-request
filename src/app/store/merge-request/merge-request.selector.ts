import { AppState } from 'src/app/app.state';
import { createSelector } from '@ngrx/store';
import * as fromMergeRequests from './merge-request.reducer';

export const selectMergeRequestState$ = (state: AppState) => state.mergeRequests;

export const selectMergeRequestEntitiesConverted$ = createSelector(
    selectMergeRequestState$, fromMergeRequests.selectMergeRequests
);

export const selectMergeRequestById$ = (id: number) => createSelector(
    selectMergeRequestState$, state => state.entities ? state.entities[id] : null
);

