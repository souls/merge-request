import { MergeRequest } from './../../model/merge-request';
import { Observable, of } from 'rxjs';
import {switchMap, map, catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { MergeRequestAction } from './merge-request.action';
import { MergeRequestService } from 'src/app/service/merge-request.service';



@Injectable()
export class MergeRequestEffects {

    @Effect()
    loadMergeRequests$: Observable<MergeRequestAction.Actions> = this.actions$.pipe(
        ofType(MergeRequestAction.ActionType.LOAD_MERGE_REQUESTS),
        switchMap(() => this.mergeRequestService.getMergeRequests()),
        map(mergeRequests => new MergeRequestAction.SuccessLoadMergeRequests(mergeRequests)),
        catchError(err => of(new MergeRequestAction.ErrorLoad(err)))
    );

    @Effect()
    addMergeRequests$: Observable<MergeRequestAction.Actions> = this.actions$.pipe(
        ofType(MergeRequestAction.ActionType.ADD_MERGE_REQUEST),
        switchMap((action: MergeRequestAction.AddMergeRequest) => this.mergeRequestService.addMergeRequest(action.payload)),
        map(mergeRequest => new MergeRequestAction.SuccessAddMergeRequest(mergeRequest)),
        catchError(err => of(new MergeRequestAction.ErrorLoad(err)))
    );

    @Effect()
    updateMergeRequests$: Observable<MergeRequestAction.Actions> = this.actions$.pipe(
        ofType(MergeRequestAction.ActionType.UPDATE_MERGE_REQUEST),
        switchMap((action: MergeRequestAction.UpdateMergeRequest) => this.mergeRequestService.updateMergeRequest(action.payload)),
        map(mergeRequest => new MergeRequestAction.SuccessUpdateMergeRequest(mergeRequest)),
        catchError(err => of(new MergeRequestAction.ErrorLoad(err)))
    );

    constructor(private actions$: Actions, public mergeRequestService: MergeRequestService) {
    }

}

