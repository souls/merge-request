import { MergeRequest } from 'src/app/model/merge-request';
import { HttpErrorResponse } from '@angular/common/http';


export namespace MergeRequestAction {

    export enum ActionType {
        LOAD_MERGE_REQUESTS = '[MERGE REQUEST] LOAD',
        SUCCESS_LOAD_MERGE_REQUESTS = '[MERGE REQUEST] SUCCESS LOAD',
        ERROR_LOAD = '[MERGE REQUEST] ERROR LOAD',
        ADD_MERGE_REQUEST = '[MERGE REQUEST] ADD',
        SUCCESS_ADD_MERGE_REQUEST = '[MERGE REQUEST] ADD SUCCESS',
        UPDATE_MERGE_REQUEST = '[MERGE REQUEST] UPDATE',
        SUCCESS_UPDATE_MERGE_REQUEST = '[MERGE REQUEST] UPDATE SUCCESS'
    }

    export class LoadMergeRequests {
        readonly type = ActionType.LOAD_MERGE_REQUESTS; //Cette ligne définie le type de la classe LoadMergeRequest
    }

    export class SuccessLoadMergeRequests {
        readonly type = ActionType.SUCCESS_LOAD_MERGE_REQUESTS;

        constructor(public payload: MergeRequest[]) {
        }
    }

    export class ErrorLoad {
        readonly type =  ActionType.ERROR_LOAD;

        constructor(public payload: HttpErrorResponse) {
        }
    }
    export class AddMergeRequest {
        readonly type = ActionType.ADD_MERGE_REQUEST;

        constructor(public payload: MergeRequest) {

        }
    }
    export class SuccessAddMergeRequest {
        readonly type = ActionType.SUCCESS_ADD_MERGE_REQUEST;

        constructor(public payload: MergeRequest) {
            console.log(payload);
        }

    }
    export class UpdateMergeRequest {
        readonly type = ActionType.UPDATE_MERGE_REQUEST;

        constructor(public payload: MergeRequest) {

        }
    }
    export class SuccessUpdateMergeRequest {
        readonly type = ActionType.SUCCESS_UPDATE_MERGE_REQUEST;

        constructor(public payload: MergeRequest) {
        }

    }
    export type Actions = LoadMergeRequests | SuccessLoadMergeRequests | ErrorLoad
                            | AddMergeRequest | SuccessAddMergeRequest | UpdateMergeRequest
                            | SuccessUpdateMergeRequest;
}
