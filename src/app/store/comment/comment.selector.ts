import { AppState } from 'src/app/app.state';
import { createSelector } from '@ngrx/store';
import * as fromComments from './comment.reducer';

export const selectCommentState$ = (state: AppState) => state.comments;

export const selectCommentEntitiesConverted$ = createSelector(
    selectCommentState$, fromComments.selectComments
);
