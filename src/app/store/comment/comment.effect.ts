import { Comment } from './../../model/comment';
import { Observable, of } from 'rxjs';
import {switchMap, map, catchError, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { CommentAction } from './comment.action';
import { MergeRequestService } from 'src/app/service/merge-request.service';

@Injectable()
export class CommentEffect {
    constructor(private actions$: Actions, public mergeRequestService: MergeRequestService) {
    }

    @Effect()
    loadComments$: Observable<CommentAction.Actions> = this.actions$.pipe(
        ofType(CommentAction.ActionType.LOAD_COMMENTS), //Renvoie une action
        switchMap((action: CommentAction.LoadComments) =>
            this.mergeRequestService.getCommentsByMergeRequestId(action.payload)
        ),
        map(comments => new CommentAction.SuccessLoadComments(comments)),
        catchError(err => of(new CommentAction.ErrorLoad(err)))
    );

    @Effect()
    addComments$: Observable<CommentAction.Actions> = this.actions$.pipe(
        ofType(CommentAction.ActionType.ADD_COMMENT),
        switchMap((action: CommentAction.AddComment) => this.mergeRequestService.addComment(action.payload)),
        map(comment => new CommentAction.SuccessAddComment(comment)),
        catchError(err => of(new CommentAction.ErrorLoad(err)))
    );
} 
