import { CommentAction } from './comment.action';
import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Comment } from '../../model/comment';

export interface CommentStateEntity extends EntityState<Comment> {
    loading: boolean;
    loaded: boolean;
    logs: {
        type: string;
        message: string;
    };
}

export const CommentAdapter: EntityAdapter<Comment> = createEntityAdapter<Comment> ({
    sortComparer: false
});

export const CommentInitialState = CommentAdapter.getInitialState({
    loading: false,
    loaded: false,
    logs: undefined
});

export const {
    selectIds: selectCommentIds,
    selectEntities: selectCommentEntities,
    selectAll: selectComments,
    selectTotal: selectCommentTotal
} = CommentAdapter.getSelectors();

export function commentReducer(state= CommentInitialState, action: CommentAction.Actions): CommentStateEntity {

    switch (action.type) {
        case CommentAction.ActionType.LOAD_COMMENTS:
            return {
                ...state,
                loading: true,
                loaded: false,
            };
        case CommentAction.ActionType.SUCCESS_LOAD_COMMENTS:
            return {
                ...CommentAdapter.addMany(action.payload, state),
                loading: false,
                loaded: true,
                logs: {
                    type: 'SUCCESS LOAD',
                    message: 'Success to load comment entities'
                }
            };
        case CommentAction.ActionType.ERROR_LOAD:
            return {
                ...state,
                loading: false,
                loaded: false,
                logs: {
                    type: 'ERROR',
                    message: action.payload.message
                }
            };
        case CommentAction.ActionType.ADD_COMMENT:
            return {
                ...state,
                loading: true,
                loaded: false,
            };
        case CommentAction.ActionType.SUCCESS_ADD_COMMENT:
            return {
                ...CommentAdapter.addOne(action.payload, state),
                loading: false,
                loaded: true,
                logs: {
                    type: 'SUCCESS ADD',
                    message: 'Success to add comment entity'
                }
            };
    }
        
}