import { Comment } from 'src/app/model/comment';
import { HttpErrorResponse } from '@angular/common/http';

export namespace CommentAction {

    export enum ActionType {
        LOAD_COMMENTS = '[ COMMENT ] LOAD ',
        SUCCESS_LOAD_COMMENTS = '[ COMMENT ] SUCCESS LOAD ',
        ADD_COMMENT = '[ COMMENT ] ADD ',
        SUCCESS_ADD_COMMENT = '[ COMMENT ] SUCCESS ADD ',
        ERROR_LOAD = '[MERGE REQUEST] ERROR LOAD',
    }

    export class ErrorLoad {
        readonly type =  ActionType.ERROR_LOAD;

        constructor(public payload: HttpErrorResponse) {
        }
    }

    export class LoadComments {
        readonly type = ActionType.LOAD_COMMENTS;
        
        constructor (public payload: number) {
            
        }

    }

    export class SuccessLoadComments {
        readonly type = ActionType.SUCCESS_LOAD_COMMENTS;

        constructor (public payload: Comment[]) {
            
        }
    }

    export class AddComment {
        readonly type = ActionType.ADD_COMMENT;

        constructor ( public payload: Comment) {

        }
    }

    export class SuccessAddComment {
        readonly type = ActionType.SUCCESS_ADD_COMMENT;

        constructor (public payload: Comment) {
            
        }
    }

    export type Actions = LoadComments | AddComment | SuccessLoadComments | SuccessAddComment | ErrorLoad ;
}
// LOAD_COMMENTS
// ADD_COMMENT