import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeRequestDetailsComponent } from './merge-request-details.component';

describe('MergeRequestDetailsComponent', () => {
  let component: MergeRequestDetailsComponent;
  let fixture: ComponentFixture<MergeRequestDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeRequestDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
