import { Comment } from './../model/comment';
import { MergeRequest } from 'src/app/model/merge-request';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { selectMergeRequestById$ } from './../store/merge-request/merge-request.selector';
import { Store, select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { MergeRequestAction } from '../store/merge-request/merge-request.action';
import { CommentAction } from '../store/comment/comment.action';
import { AppState } from '../app.state';

@Component({
  selector: 'app-merge-request-details',
  templateUrl: './merge-request-details.component.html',
  styleUrls: ['./merge-request-details.component.css']
})
export class MergeRequestDetailsComponent implements OnInit {
  form: FormGroup;
  mergeRequest$: Observable<MergeRequest>;
  id: number;

  constructor(private router: Router, private store: Store<AppState>,
              private route: ActivatedRoute, private formBuilder: FormBuilder) {

       this.form = this.formBuilder.group({
      state: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      author: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ])
    });
    }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.mergeRequest$ = this.store.pipe(select(selectMergeRequestById$(this.id)));
  }

  backToMergeRequestList() {
    this.router.navigateByUrl('/');
  }

  updateMergeRequest(mergeRequest: MergeRequest) {
    console.log(mergeRequest);
    this.store.dispatch(new MergeRequestAction.UpdateMergeRequest(mergeRequest));
    this.router.navigateByUrl('/');
  }

/*
  addComment () {
    this.store.dispatch(new CommentAction.AddComment({
      id: null,
      author: this.form.get('author').value,
      message: document.getElementById('comment').nodeValue
    }));
  }*/
}
