import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeRequestListComponent } from './merge-request-list.component';

describe('MergeRequestListComponent', () => {
  let component: MergeRequestListComponent;
  let fixture: ComponentFixture<MergeRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
