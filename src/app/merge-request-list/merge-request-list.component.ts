import { selectMergeRequestEntitiesConverted$ } from './../store/merge-request/merge-request.selector';
import { MergeRequestAction } from './../store/merge-request/merge-request.action';
import { Observable } from 'rxjs';
import { MergeRequest } from './../model/merge-request';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '../app.state';
import { MergeRequestService } from './../service/merge-request.service'

@Component({
  selector: 'app-merge-request-list',
  templateUrl: './merge-request-list.component.html',
  styleUrls: ['./merge-request-list.component.css']
})
export class MergeRequestListComponent implements OnInit {

  mergeRequests$: Observable<MergeRequest[]>;
  id: number;

  constructor(private store: Store<AppState>, private mergeRequestService: MergeRequestService) {
    this.store.dispatch(new MergeRequestAction.LoadMergeRequests());
  }

  ngOnInit() {
    this.mergeRequests$ = this.store.pipe(select(selectMergeRequestEntitiesConverted$));
  }

}
