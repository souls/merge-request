import { Component, OnInit, Input } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from '../app.state';
import { CommentAction } from '../store/comment/comment.action';
import { Observable } from 'rxjs';
import { Comment } from '../model/comment';
import { selectCommentEntitiesConverted$ } from '../store/comment/comment.selector';


@Component({
  selector: 'app-comment-list',
  templateUrl: './comment-list.component.html',
  styleUrls: ['./comment-list.component.css']
})
export class CommentListComponent implements OnInit {
  @Input()
  mergeRequestId: number;

  comments$: Observable<Comment[]>;

  constructor(private store: Store<AppState>) {
    this.store.dispatch(new CommentAction.LoadComments(this.mergeRequestId));
  }

  ngOnInit() {
    this.comments$ = this.store.pipe(select(selectCommentEntitiesConverted$));
  }

}
