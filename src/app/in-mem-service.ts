import { Comment } from './model/comment';
import { MergeRequest } from './model/merge-request';
import { InMemoryDbService } from 'angular-in-memory-web-api';


export class InMemService implements InMemoryDbService {

    createDb() {

        const comments: Comment[] = [
            {
                id: 1,
                author: 'Souleymane BA',
                message: 'Ceci est un commentaire',
                mergeRequestId: 1
            },
            {
                id: 2,
                author: 'Abdoulaye DAMA',
                message: 'Ceci est un message',
                mergeRequestId: 1
            }
        ];

        const mergeRequests: MergeRequest[] = [
            {
                id: 1,
                title: 'Update tasks',
                author: 'Souleymane BA',
                date: '2020-01-01T00:00:00.000Z',
                state: 'OPENED'
            },
            {
                id: 2,
                title: 'Feature help assistante',
                author: 'Abdoulaye DAMA',
                date: '2020-04-01T15:12:00.000Z',
                state: 'OPENED'
            }
        ];

        return {comments, 'merge-request': mergeRequests};
    }
}
