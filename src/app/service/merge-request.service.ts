import { MergeRequest } from './../model/merge-request';
import { Comment } from './../model/comment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import { tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class MergeRequestService {

  // '/merge-request' <=> http://localhost:<port>/merge-request
  private mergeRequestUrl = 'api/merge-request';
  private commentUrl = 'api/comments';

  constructor(private http: HttpClient) { }

  getMergeRequests(): Observable<MergeRequest[]> {
    return this.http.get<MergeRequest[]>(this.mergeRequestUrl);
  }

  getCommentsByMergeRequestId(id: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${this.commentUrl}?mergeRequestId=${id}`); // Manière de variabiliser une chaine de caractère
  }

  addComment(comment: Comment): Observable<Comment> {
    return this.http.post<Comment>(this.commentUrl, comment);
  }

  addMergeRequest(mergeRequest: MergeRequest): Observable<MergeRequest> {
    return this.http.post<MergeRequest>(this.mergeRequestUrl, mergeRequest);
  }

  updateMergeRequest(mergeRequest: MergeRequest): Observable<MergeRequest> {
    const url = `${this.mergeRequestUrl}/${mergeRequest.id}`;
    this.http.put<MergeRequest>(`${this.mergeRequestUrl}/${url}`, mergeRequest);

    // using stupid syntax to fix bug because the
    // in-memory-web-api backend doesn't return response.
    return of(mergeRequest);
  }
}
