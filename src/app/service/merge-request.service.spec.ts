import { TestBed } from '@angular/core/testing';

import { MergeRequestService } from './merge-request.service';

describe('MergeRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MergeRequestService = TestBed.get(MergeRequestService);
    expect(service).toBeTruthy();
  });
});
