import { environment } from './../environments/environment.prod';
import { REDUCER_TOKEN, appEffects, getReducers } from './app.state';
import { InMemService } from './in-mem-service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MergeRequestListComponent } from './merge-request-list/merge-request-list.component';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { HttpClientModule } from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { MergeRequestAddComponent } from './merge-request-add/merge-request-add.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MergeRequestDetailsComponent } from './merge-request-details/merge-request-details.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { CommentDetailComponent } from './comment-detail/comment-detail.component';
import { CommentListComponent } from './comment-list/comment-list.component';

@NgModule({
  declarations: [
    AppComponent,
    MergeRequestListComponent,
    MergeRequestAddComponent,
    MergeRequestDetailsComponent,
    CommentDetailComponent,
    CommentListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    InMemoryWebApiModule.forRoot(InMemService),
    StoreModule.forRoot(REDUCER_TOKEN),
    EffectsModule.forRoot(appEffects),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
  ],
  providers: [
    {provide: REDUCER_TOKEN, useFactory: getReducers}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
