

export interface Comment {
    id: number;
    author: string;
    message: string;
    mergeRequestId: number;
}
