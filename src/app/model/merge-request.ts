
import './comment';

export interface MergeRequest {
    id: number;
    title: string;
    author: string;
    date: string;
    state: string;
}
