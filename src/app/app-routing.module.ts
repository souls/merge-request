import { MergeRequestListComponent } from './merge-request-list/merge-request-list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MergeRequestAddComponent } from './merge-request-add/merge-request-add.component';
import { MergeRequestDetailsComponent} from './merge-request-details/merge-request-details.component';

const routes: Routes = [
  {path: '', component: MergeRequestListComponent},
  {path: 'add', component: MergeRequestAddComponent},
  {path: ':id', component: MergeRequestDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
