import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { MergeRequest } from '../model/merge-request';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import { MergeRequestAction } from '../store/merge-request/merge-request.action';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-merge-request-add',
  templateUrl: './merge-request-add.component.html',
  styleUrls: ['./merge-request-add.component.css']
})
export class MergeRequestAddComponent implements OnInit {
  form: FormGroup;
  mergeRequest: MergeRequest;
  id: number; 

  constructor(private formBuilder: FormBuilder, private store: Store<AppState>,
              private router: Router, private route: ActivatedRoute) {
    this.form = this.formBuilder.group({
      author: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ]),
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ])
    });
  }

  ngOnInit() {
    
  }

  addMergeRequest() {
    this.store.dispatch(new MergeRequestAction.AddMergeRequest({
      id: null,
      author: this.form.get('author').value,
      title: this.form.get('title').value,
      date: new Date().toISOString(),
      state: 'OPENED'
    }));
    this.router.navigateByUrl('/');
  }

}
