import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MergeRequestAddComponent } from './merge-request-add.component';

describe('MergeRequestAddComponent', () => {
  let component: MergeRequestAddComponent;
  let fixture: ComponentFixture<MergeRequestAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MergeRequestAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MergeRequestAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
