import { InjectionToken } from '@angular/core';
import { ActionReducerMap } from '@ngrx/store';
import { mergeRequestReducer, MergeRequestStateEntity } from './store/merge-request/merge-request.reducer';
import { MergeRequestEffects } from './store/merge-request/merge-request.effect';
import { commentReducer, CommentStateEntity } from './store/comment/comment.reducer';
import { CommentEffect } from './store/comment/comment.effect';


// TODO: add commentReducer that you defines into comment.reducer.ts
const reducers = {
  mergeRequests: mergeRequestReducer,
  comments: commentReducer
};

// TODO: add CommentStateEntity that you defines into comment.reducer.ts
export interface AppState {
    mergeRequests: MergeRequestStateEntity;
    comments: CommentStateEntity;
}

export function getReducers() {
    return reducers;
}

// AOT
export const REDUCER_TOKEN = new InjectionToken<ActionReducerMap<AppState>>('Registered Reducers');

export const appEffects = [
    MergeRequestEffects,
    CommentEffect
];
